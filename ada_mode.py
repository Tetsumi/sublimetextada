#
# Title:  Ada mode plugin
#
# Authors: 
# 		 Tetsumi <tetsumi@vmail.me>
#
# Version: 1.0
#
# Copyright (c) 2013 Tetsumi
#
# Permission to copy, use, modify, sell and distribute this
# software is granted. This software is provided "as is" without
# express or implied warranty, and with no claim as to its
# suitability for any purpose.
#

import sublime
import sublime_plugin

# update project folders =>
# self.window.run_command("refresh_folder_list")

def g_is_enabled(gpr_file):
	return gpr_file != None and len(gpr_file) > 0

class OpenGprCommand(sublime_plugin.WindowCommand):
	def run(self):
		def on_done(str):
			if str.endswith(".gpr"):
				self.window.active_view().settings().set('GPR_File', str)
			else:
				sublime.status_message("Error: Filepath doesnt end with .gpr")			
		self.window.show_input_panel("Enter GPR file path", "/", on_done, None, None)

class CloseGprCommand(sublime_plugin.WindowCommand):
	def is_enabled(self):
		gpr_file = self.window.active_view().settings().get('GPR_File')
		return g_is_enabled(gpr_file)
	def run(self):
		self.window.active_view().settings().set('GPR_File', None)

class BuildGprCommand(sublime_plugin.WindowCommand):
	def is_enabled(self):
		gpr_file = self.window.active_view().settings().get('GPR_File')
		return g_is_enabled(gpr_file)
	def run(self):
		gpr_file = self.window.active_view().settings().get('GPR_File')
		args =  { 
		"cmd": ["gnatmake", "-d", "-P", gpr_file], 
		"file_regex": "",
		"line_regex": "",
		"working_dir": "",         
		"encoding": "utf-8",
		"env": {},
		"quiet": False,
		"kill": False, 
		}
		self.window.run_command("exec", args)

